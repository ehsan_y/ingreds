import { IsNumber, IsObject, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class StockDto  {

    @IsString()
    @ApiProperty({ example: '' })
    ingredienId: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ example: 3 })
    increase: number

    @IsOptional()
    @IsNumber()
    @ApiProperty({ example: 2 })
    decrease: number
}
