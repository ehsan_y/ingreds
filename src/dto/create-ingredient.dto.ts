import { IsNumber, IsObject, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateIngredientDto  {

    @IsString()
    @ApiProperty({ example: "bread" })
    name: string

    @IsNumber()
    @ApiProperty({ example: 1 })
    categoryId: number

    @IsNumber()
    @ApiProperty({ example: 1 })
    measurementId: number

    @IsString()
    @ApiProperty({ example: {
       "name": "food-provider",
       "price": 10000,
      }
    })
    Suppliers: string[]

    @IsNumber()
    @ApiProperty({ example: 13 })
    stock: number
}
