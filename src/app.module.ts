import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      database: "ingredients",
      //type: "postgres",
      type: "mongodb",
      url: process.env.MONGODB_URL,
      synchronize: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      logger: "debug",
      entities: [
      ]
    }),
   
    ],
})
export class AppModule {

}



