import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ObjectId, Repository } from 'typeorm';
import { Ingredient } from './models/ingredients.entity';
import { Measurement } from './models/measurement.entity';
import { Suppelier } from './models/suppelier.entity';
import { StockDto } from './dto/stock.dto';
import { CreateIngredientDto } from './dto/create-ingredient.dto';

@Injectable()


export class AppService {

  constructor(
    @InjectRepository(Ingredient)
    protected readonly IngredientRepository: Repository<Ingredient>,
    @InjectRepository(Measurement) 
    protected readonly MeasureRepository: Repository<Measurement>,
    @InjectRepository(Measurement) 
    protected readonly supplierRepository: Repository<Suppelier>,
  ) {
  }
  getList(take?: string, skip?: string): Object {
    take = take == null ? '1' : take;
    skip = skip == null ? '0' : skip;
    let x = { take: +take, skip: +skip };
    let ingredient = this.IngredientRepository.findAndCount(x)
    return ingredient;
  }

  public async create(body) {
    let ingredient = this.IngredientRepository.create(body)
    let result = await this.IngredientRepository.save(ingredient)
    return result
  }

  public async updateStock(body: StockDto){
    let indredient = await this.IngredientRepository.findOne({
      where: { _id: new ObjectId(body.ingredienId) }
    });
    let stock: number;
    if(body.hasOwnProperty('decrease'))
      stock -= indredient.stock
    if(body.hasOwnProperty('increase'))
      stock += indredient.stock

    let id = indredient._id
    return await this.IngredientRepository.update(id, {stock: stock})
    
  }

}
