import {
            Column,
            CreateDateColumn,
            DeleteDateColumn,
            Entity,
            ObjectId,
            ObjectIdColumn,
            PrimaryGeneratedColumn
            } from "typeorm";
        
        @Entity({ name: "suppeliers" })
        
        export class Suppelier { 
            @PrimaryGeneratedColumn()
            @ObjectIdColumn()
            _id: ObjectId
        
            @Column()
            name: number
        }
        
        