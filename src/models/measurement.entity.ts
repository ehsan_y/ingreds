import {
            Column,
            Entity,
            ObjectId,
            ObjectIdColumn,
            PrimaryGeneratedColumn
            } from "typeorm";
        
        @Entity({ name: "measurements" })
        
        export class Measurement { 
            @PrimaryGeneratedColumn()
            @ObjectIdColumn()
            _id: ObjectId
        
            @Column()
            unit: number
        
            @Column()
            symbol: number
        }
        
        