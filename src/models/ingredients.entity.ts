import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    ObjectId,
    ObjectIdColumn,
    PrimaryGeneratedColumn
    } from "typeorm";

@Entity({ name: "ingredients" })

export class Ingredient { 
    @PrimaryGeneratedColumn()
    @ObjectIdColumn()
    _id: ObjectId

    @Column()
    name: number

    @Column()
    categoryId: number

    @Column()
    measurementId: number

    @Column()
    SuppliersId: string[]

    @Column()
    stock: number

    @CreateDateColumn()
    created_at: Date

    @DeleteDateColumn()
    public deletedAt: Date|undefined;
}

