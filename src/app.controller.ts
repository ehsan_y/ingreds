import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { CreateIngredientDto } from './dto/create-ingredient.dto';
import { StockDto } from './dto/stock.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('ingredient')
  public async getList(@Query() query) {
    try{
    let result: Object = this.appService.getList(
      query.perpage,
      query.page,
      );
      return result;
    } catch (error) {
      return error;
    }
  }

  @Post('ingredient/stock')
  async changeStock(
    @Body() stockDto: StockDto,
  ) {
    try {
      let result: Object = await this.appService.updateStock(stockDto);
      return result;
    } catch (error) {
      return error;
    }
  }
  @Post('ingredient')
  async create(
    @Body() createIngredientDto: CreateIngredientDto,
  ) {
    try {
      let result: Object = await this.appService.create(createIngredientDto);
      return result;
    } catch (error) {
      return error;
    }
  }
}
